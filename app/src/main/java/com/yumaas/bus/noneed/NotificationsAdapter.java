package com.yumaas.bus.noneed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.bus.R;


public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    Context context;


    public NotificationsAdapter(Context context) {
        this.context = context;
    }


    @Override
    public NotificationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        NotificationsAdapter.ViewHolder viewHolder = new NotificationsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NotificationsAdapter.ViewHolder holder, final int position) {

        holder.ll.setBackgroundColor(holder.itemView.getContext().getResources().getColor(((position%2)==1)?R.color.colorGrayLight2:R.color.colorWhite));

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout ll;

        public ViewHolder(View itemView) {
            super(itemView);

            ll=itemView.findViewById(R.id.ll);
        }
    }
}
