package com.yumaas.bus.noneed;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.yumaas.bus.R;

public class CustomDialogClass extends Dialog   {

    public Activity c;
    public Dialog d;
    TextView title,details;
    public Button yes, no;

    public CustomDialogClass(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        title = findViewById(R.id.txt_dia);
        details = findViewById(R.id.txt_details);

        yes.setVisibility(View.GONE);
        no.setVisibility(View.GONE);

        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        getWindow().setLayout(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

    }

    public void setTitle(String title) {
       this.title.setText(title);
    }

    public void setMessage(String details) {
        this.details.setText(details);
    }

    public void setButton(String text , View.OnClickListener onClickListener){
        yes.setVisibility(View.VISIBLE);
        yes.setText(text);
        yes.setOnClickListener(onClickListener);
    }

    public void setButton2(String text , View.OnClickListener onClickListener){
        no.setVisibility(View.VISIBLE);
        no.setText(text);
        no.setOnClickListener(onClickListener);
    }


}

