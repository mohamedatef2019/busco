package com.yumaas.bus.noneed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.bus.R;


public class TestingAdapter extends RecyclerView.Adapter<TestingAdapter.ViewHolder> {

    Context context;
   String []images={
           "https://app.pelorous.com/public/cms/209/432/633/1935/mkNIT3_web.jpg",
           "https://www.moviemaker.com/wp-content/uploads/2020/11/Kiera-Allen-Run.jpg"
   ,"https://img.freepik.com/free-photo/woman-wheelchair-taking-selfie-with-smartphone_23-2148726993.jpg?size=626&ext=jpg"
   ,"https://www.localgov.co.uk/images/teaser/disabled-child-playing_637236598382303495.jpg"
   ,"https://firststepsenterprise.co.uk/wp-content/uploads/2019/02/SEN.jpg"

   ,"https://macdndev.azureedge.net/genesis-temp/6/9/7/4/7/6/697476573704a8cf44204c3b46f7ef86f743232a.jpg"
   ,"https://salemua.org/wp-content/uploads/2020/04/pt-5-1024x768.jpg"
   ,"https://www.fhi360.org/sites/default/files/media/images/content/feature-ctd-adaptive-tech-400X267.jpg"};

    String []names={"Ahmed Khaled"
    ,"Cloud Change"
    ,"Master Amazing"
    ,"Egypt Change"
    ,"Ready"
    ,"Yumaas"
    ,"Green Cut"
    ,"Orange Money"};

    String []details={"10 USD"
            ,"5 %"
            ,"5 % + 1 USD"
            ,"Free"
            ,"2 %"
            ,"3 %"
            ,"4 %"
            ,"2 %"};


    public TestingAdapter(Context context) {
        this.context = context;
    }


    @Override
    public TestingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_branch, parent, false);
        TestingAdapter.ViewHolder viewHolder = new TestingAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TestingAdapter.ViewHolder holder, final int position) {
        holder.name.setText("Child Name");
        holder.details.setText("");

        Picasso.get().load(images[position]).into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.replaceFragment(view.getContext(), new ChildDetaillsFragment(), "CompanyDetailsFragment");

            }
        });


    }

    @Override
    public int getItemCount() {
        return names.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,details;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name=itemView.findViewById(R.id.name);
            this.details=itemView.findViewById(R.id.details);
            this.image=itemView.findViewById(R.id.image);

        }
    }
}
