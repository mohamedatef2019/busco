package com.yumaas.bus.noneed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;
import com.yumaas.bus.R;


public class SliderAdapterBarber extends SliderViewAdapter<SliderAdapterBarber.SliderAdapterVH> {

    private Context context;
    private String[] images;
    public SliderAdapterBarber(Context context,String [] images) {
        this.context = context;
        this.images=images;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hall_image, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {

        Picasso.get().load(images[position]).into(viewHolder.imageView);

    }

    @Override
    public int getCount() {
        return images==null?0:images.length;
    }

    public static  class SliderAdapterVH extends SliderViewAdapter.ViewHolder  {

        ImageView imageView;

        public SliderAdapterVH(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);

        }
    }
}