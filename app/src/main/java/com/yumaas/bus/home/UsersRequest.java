package com.yumaas.bus.home;

import com.google.gson.annotations.SerializedName;
import com.yumaas.bus.base.DefaultRequest;


public class UsersRequest extends DefaultRequest {



    @SerializedName("owner_id")
    private String ownerId ;

    @SerializedName("code")
    private String code ;


    public UsersRequest(String method) {
        super(method);
    }
    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
