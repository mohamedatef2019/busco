package com.yumaas.bus.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.bus.R;
import com.yumaas.bus.home.response.ChildsItem;
import com.yumaas.bus.noneed.FragmentHelper;


import java.util.List;


public class ChildsAdapter extends RecyclerView.Adapter<ChildsAdapter.ViewHolder> {

    Context context;
    List<ChildsItem>childsItems;
    public ChildsAdapter(Context context,List<ChildsItem>childsItems) {
        this.context = context;
        this.childsItems=childsItems;
    }


    @Override
    public ChildsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_enter_code, parent, false);
        ChildsAdapter.ViewHolder viewHolder = new ChildsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChildsAdapter.ViewHolder holder, final int position) {
        holder.name.setText(childsItems.get(position).getName());
        holder.details.setText(childsItems.get(position).getPhone());

        Picasso.get().load(childsItems.get(position).getImage()).into(holder.image);

        holder.itemView.setOnClickListener(view -> {
         });


    }

    @Override
    public int getItemCount() {
        return childsItems==null?0:childsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,details;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name=itemView.findViewById(R.id.name);
            this.details=itemView.findViewById(R.id.details);
            this.image=itemView.findViewById(R.id.image);

        }
    }
}
