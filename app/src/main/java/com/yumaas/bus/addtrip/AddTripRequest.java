package com.yumaas.bus.addtrip;

import com.google.gson.annotations.SerializedName;
import com.yumaas.bus.base.DefaultRequest;


public class AddTripRequest extends DefaultRequest {


    @SerializedName("name")
    private String name;

    @SerializedName("details")
    private String details;


    @SerializedName("trip_from")
    private String tripFrom;


    @SerializedName("trip_to")
    private String tripTo;


    @SerializedName("driver")
    private String driver;

    @SerializedName("price")
    private String price;

    @SerializedName("comment")
    private String comment;

    @SerializedName("number")
    private String number;


    @SerializedName("company_id")
    private String companyId="0";

    public AddTripRequest(String method) {
        super(method);
    }


    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setTripFrom(String tripFrom) {
        this.tripFrom = tripFrom;
    }

    public void setTripTo(String tripTo) {
        this.tripTo = tripTo;
    }

    @Override
    public void setMethod(String method) {
        super.setMethod(method);
    }

    public String getComment() {
        return comment;
    }

    public String getDetails() {
        return details;
    }

    public String getDriver() {
        return driver;
    }

    public String getPrice() {
        return price;
    }

    public String getTripFrom() {
        return tripFrom;
    }

    public String getTripTo() {
        return tripTo;
    }

    @Override
    public String getMethod() {
        return super.getMethod();
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }
}
