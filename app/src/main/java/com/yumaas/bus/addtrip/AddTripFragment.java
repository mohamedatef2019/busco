package com.yumaas.bus.addtrip;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.bus.R;
import com.yumaas.bus.addtrip.response.AreasResponse;
import com.yumaas.bus.base.DefaultResponse;
import com.yumaas.bus.base.UserPreferenceHelper;
import com.yumaas.bus.base.Validate;
import com.yumaas.bus.base.volleyutils.ConnectionHelper;
import com.yumaas.bus.base.volleyutils.ConnectionListener;
import com.yumaas.bus.drivers.DriversAdapter;
import com.yumaas.bus.drivers.DriversFragment;
import com.yumaas.bus.drivers.response.DriversResponse;
import com.yumaas.bus.home.UsersRequest;
import com.yumaas.bus.login.UserResponse;
import com.yumaas.bus.noneed.FragmentHelper;
import com.yumaas.bus.register.RegisterRequest;
import com.yumaas.bus.trips.TripsFragment;

public class AddTripFragment extends Fragment {
    EditText name,details,tripFrom,tripTo,driver,price,number;
    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add_trip, container, false);


        details=rootView.findViewById(R.id.details);
        name=rootView.findViewById(R.id.name);
        tripFrom=rootView.findViewById(R.id.from);
        tripTo=rootView.findViewById(R.id.to);
        driver=rootView.findViewById(R.id.driver);
        price=rootView.findViewById(R.id.price);
        number=rootView.findViewById(R.id.number);

        getDrivers();
        getAreas();

        clickListeners();

        return rootView;
    }


    private void clickListeners(){

        tripFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(areasResponse!=null) {
                    String list[] = new String[areasResponse.getAreas().size()];
                    for (int i = 0; i < list.length; i++) {
                        list[i] = areasResponse.getAreas().get(i).getName();
                    }
                    popUpMenu(list,tripFrom);
                }
            }
        });

        tripTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(areasResponse!=null) {
                    String list[] = new String[areasResponse.getAreas().size()];
                    for (int i = 0; i < list.length; i++) {
                        list[i] = areasResponse.getAreas().get(i).getName();
                    }
                    popUpMenu(list,tripTo);
                }
            }
        });


        driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(driversResponse!=null) {
                    String list[] = new String[driversResponse.getDrivers().size()];
                    for (int i = 0; i < list.length; i++) {
                        list[i] = driversResponse.getDrivers().get(i).getName();
                    }
                    popUpMenu(list,driver);
                }
            }
        });



        rootView.findViewById(R.id.btn).setOnClickListener(view -> {
            if(Validate.isEmpty(name.getText().toString())||
                    Validate.isEmpty(tripTo.getText().toString())||
                    Validate.isEmpty(driver.getText().toString())||
                    Validate.isEmpty(price.getText().toString())||
                    Validate.isEmpty(details.getText().toString())||
                    Validate.isEmpty(number.getText().toString())||
                    Validate.isEmpty(tripFrom.getText().toString())){
                Toast.makeText(requireActivity(), "please fill all fields", Toast.LENGTH_SHORT).show();
            }else {
                registerApi();
            }
        });
    }


    public void registerApi(){

        AddTripRequest addTripRequest = new AddTripRequest("add_trip");
        addTripRequest.setName(name.getText().toString());
        addTripRequest.setDetails(details.getText().toString());
        addTripRequest.setNumber(number.getText().toString());
        addTripRequest.setDriver(driver.getText().toString());
        addTripRequest.setTripFrom(tripFrom.getText().toString());
        addTripRequest.setTripTo(tripTo.getText().toString());
        addTripRequest.setPrice(price.getText().toString());
        addTripRequest.setCompanyId(UserPreferenceHelper.getUserDetails().getId()+"");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                DefaultResponse defaultResponse = (DefaultResponse)response;
                if (defaultResponse.getStatus()==101){
                    FragmentHelper.popAllFragments(requireActivity());
                    FragmentHelper.replaceFragment(requireActivity(), new TripsFragment(), "TripsFragment");
                }
            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(addTripRequest, DefaultResponse.class);
    }


    private void popUpMenu(String[]list,EditText editText){

        PopupMenu popup = new PopupMenu(requireActivity(), editText);
        for(int i=0; i<list.length; i++) {
            popup.getMenu().add(list[i]);
        }


        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                editText.setText(item.getTitle());
                return true;
            }
        });

        popup.show(); //showing popup menu
    }

    DriversResponse driversResponse;

    public void getDrivers(){

        UsersRequest loginRequest = new UsersRequest("drivers");
        loginRequest.setOwnerId(UserPreferenceHelper.getUserDetails().getId()+"");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                driversResponse = (DriversResponse) response;


            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, DriversResponse.class);
    }

    AreasResponse  areasResponse;
    public void getAreas(){

        UsersRequest loginRequest = new UsersRequest("areas");
        loginRequest.setOwnerId(UserPreferenceHelper.getUserDetails().getId()+"");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                 areasResponse = (AreasResponse) response;


            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, AreasResponse.class);
    }


}