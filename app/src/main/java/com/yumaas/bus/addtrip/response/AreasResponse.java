package com.yumaas.bus.addtrip.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AreasResponse{

	@SerializedName("areas")
	private List<AreasItem> areas;

	@SerializedName("state")
	private int state;

	public void setAreas(List<AreasItem> areas){
		this.areas = areas;
	}

	public List<AreasItem> getAreas(){
		return areas;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}
}