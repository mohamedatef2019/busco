package com.yumaas.bus.trips.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TripsResponse{

	@SerializedName("trips")
	private List<TripsItem> trips;

	@SerializedName("state")
	private int state;

	public void setTrips(List<TripsItem> trips){
		this.trips = trips;
	}

	public List<TripsItem> getTrips(){
		return trips;
	}

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}
}