package com.yumaas.bus.trips;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.bus.R;
import com.yumaas.bus.base.UserPreferenceHelper;
import com.yumaas.bus.base.volleyutils.ConnectionHelper;
import com.yumaas.bus.base.volleyutils.ConnectionListener;
import com.yumaas.bus.drivers.DriversAdapter;
import com.yumaas.bus.drivers.response.DriversResponse;
import com.yumaas.bus.home.UsersRequest;
import com.yumaas.bus.trips.response.TripsResponse;


public class TripsFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);





        getTrips();

        return rootView;
    }

    public void getTrips(){

        UsersRequest loginRequest = new UsersRequest("trips_by_company");
        loginRequest.setOwnerId(UserPreferenceHelper.getUserDetails().getId()+"");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                TripsResponse tripsResponse = (TripsResponse) response;
                final TripsAdapter teachersAdapter = new TripsAdapter(getActivity(),tripsResponse.getTrips());
                final RecyclerView programsList = rootView.findViewById(R.id.recycler_view);
                programsList.setLayoutManager(new LinearLayoutManager(getActivity()));
                programsList.setAdapter(teachersAdapter);
            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, TripsResponse.class);
    }
}