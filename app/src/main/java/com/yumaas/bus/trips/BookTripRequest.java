package com.yumaas.bus.trips;

import com.google.gson.annotations.SerializedName;
import com.yumaas.bus.base.DefaultRequest;
import com.yumaas.bus.base.UserPreferenceHelper;

public class BookTripRequest  extends DefaultRequest {

    @SerializedName("user_id")
    private String userId ;

    @SerializedName("trip_id")
    private String tripId ;



    public BookTripRequest(String method) {
        super(method);
    }

    @Override
    public void setMethod(String method) {
        super.setMethod(method);
    }

    @Override
    public String getMethod() {
        return super.getMethod();
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTripId() {
        return tripId;
    }

    public String getUserId() {
        return userId;
    }


}