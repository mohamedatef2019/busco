package com.yumaas.bus.trips;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.bus.R;
import com.yumaas.bus.base.DefaultRequest;
import com.yumaas.bus.base.DefaultResponse;
import com.yumaas.bus.base.UserPreferenceHelper;
import com.yumaas.bus.base.volleyutils.ConnectionHelper;
import com.yumaas.bus.base.volleyutils.ConnectionListener;
import com.yumaas.bus.home.UsersRequest;
import com.yumaas.bus.noneed.ScannerActivity;
import com.yumaas.bus.noneed.SplashScreenActivity;
import com.yumaas.bus.trips.response.TripsItem;
import com.yumaas.bus.trips.response.TripsResponse;

import java.util.List;


public class UserTripsAdapter extends RecyclerView.Adapter<UserTripsAdapter.ViewHolder> {

    Context context;
    String tripId;
    List<TripsItem> tripsItems;




    public UserTripsAdapter(Context context, List<TripsItem>tripsItems) {
        this.context = context;
        this.tripsItems=tripsItems;
    }


    @Override
    public UserTripsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trip, parent, false);
        UserTripsAdapter.ViewHolder viewHolder = new UserTripsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UserTripsAdapter.ViewHolder holder, final int position) {

        holder.name.setText("Trip Name : "+tripsItems.get(position).getName()
                +"\n"+ "Driver Name : "+tripsItems.get(position).getDriver());

        holder.details.setText("From : "+tripsItems.get(position).getTripFrom()+"\n"+
                "To   : "+tripsItems.get(position).getTripTo()

                +"\n"+
                "Capacity : "+tripsItems.get(position).getNumber()+" Person"

                +"\n"+
                "Price   : "+tripsItems.get(position).getPrice()+" EGP"

        );
        holder.add.setText("Book Trip");

        holder.add.setOnClickListener(view -> selectPayment(view.getContext(),tripsItems.get(position).getId()));


    }

    private void selectPayment(Context context,String id){
        tripId=id;
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setMessage("Enter Your Payment");
        alert.setTitle("Payment Type");



        alert.setPositiveButton("Scan Code", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
              Intent intent = new Intent(context, ScannerActivity.class);
                ((Activity)context).startActivityForResult(intent,1102);

            }
        });

        alert.setNegativeButton("Visa", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                dialog.cancel();
                showVisa(context);
            }
        });

        alert.show();
    }

    private void showVisa(Context context){
        LayoutInflater factory = LayoutInflater.from(context);
        final View deleteDialogView = factory.inflate(R.layout.visa_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(context).create();
        deleteDialog.setView(deleteDialogView);

        deleteDialogView.findViewById(R.id.pay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
                deleteDialog.cancel();
                payNow();
                deleteDialog.dismiss();
            }
        });
        deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        deleteDialog.getWindow().setLayout(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        deleteDialog.show();
    }


    private void payNow(){

        BookTripRequest loginRequest = new BookTripRequest("book_trip");
        loginRequest.setUserId(UserPreferenceHelper.getUserDetails().getId()+"");
        loginRequest.setTripId(tripId);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                DefaultResponse defaultResponse = (DefaultResponse) response;
                if(defaultResponse.getStatus()==101) {
                    for(int i=0; i<tripsItems.size(); i++){
                        if(tripsItems.get(i).getId().equals(tripId)){
                            tripsItems.get(i).setNumber(""+( Integer.parseInt(tripsItems.get(i).getNumber())-1));
                            notifyDataSetChanged();
                        }
                    }
                    Toast.makeText(context, "Booked Successfully", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(context, "Sorry This trip is completed", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, DefaultResponse.class);
    }

    @Override
    public int getItemCount() {
        return tripsItems==null?0:tripsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView name,details,add;

        public ViewHolder(View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            details=itemView.findViewById(R.id.details);
            imageView=itemView.findViewById(R.id.image);
            add=itemView.findViewById(R.id.add);
        }
    }
}
