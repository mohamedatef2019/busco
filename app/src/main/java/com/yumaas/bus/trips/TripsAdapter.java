package com.yumaas.bus.trips;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.bus.R;
import com.yumaas.bus.drivers.response.DriversItem;
import com.yumaas.bus.noneed.SplashScreenActivity;
import com.yumaas.bus.trips.response.TripsItem;

import java.util.List;


public class TripsAdapter extends RecyclerView.Adapter<TripsAdapter.ViewHolder> {

    Context context;

    List<TripsItem> tripsItems;




    public TripsAdapter(Context context, List<TripsItem>tripsItems) {
        this.context = context;
        this.tripsItems=tripsItems;
    }


    @Override
    public TripsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trip, parent, false);
        TripsAdapter.ViewHolder viewHolder = new TripsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TripsAdapter.ViewHolder holder, final int position) {

        holder.name.setText("Trip Name : "+tripsItems.get(position).getName()
                +"\n"+ "Driver Name : "+tripsItems.get(position).getDriver());

        holder.details.setText("From : "+tripsItems.get(position).getTripFrom()+"\n"+
                "To   : "+tripsItems.get(position).getTripTo()

                +"\n"+
                "Capacity : "+tripsItems.get(position).getNumber()+" Person"

                +"\n"+
                "Price   : "+tripsItems.get(position).getPrice()+" EGP"

        );


        holder.add.setOnClickListener(view -> addComment(view.getContext()));


    }

    private void addComment(Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        final EditText edittext = new EditText(context);
        alert.setMessage("Enter Your Comment");
        alert.setTitle("Add Comment");

        alert.setView(edittext);

        alert.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                String YouEditTextValue = edittext.getText().toString();

                sendNotification("Bus Travel",YouEditTextValue,context);

            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        alert.show();
    }

    private void sendNotification(String title, String body,Context context) {
        Intent intent = new Intent(context, SplashScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "channelId";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    public int getItemCount() {
        return tripsItems==null?0:tripsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView name,details,add;

        public ViewHolder(View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            details=itemView.findViewById(R.id.details);
            imageView=itemView.findViewById(R.id.image);
            add=itemView.findViewById(R.id.add);
        }
    }
}
