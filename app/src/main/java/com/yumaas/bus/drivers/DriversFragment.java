package com.yumaas.bus.drivers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.bus.R;
import com.yumaas.bus.base.UserPreferenceHelper;
import com.yumaas.bus.base.volleyutils.ConnectionHelper;
import com.yumaas.bus.base.volleyutils.ConnectionListener;
import com.yumaas.bus.drivers.response.DriversResponse;
import com.yumaas.bus.home.UsersRequest;
import com.yumaas.bus.noneed.TestingAdapter;


public class DriversFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);





        getDrivers();

        return rootView;
    }

    public void getDrivers(){

        UsersRequest loginRequest = new UsersRequest("drivers");
        loginRequest.setOwnerId(UserPreferenceHelper.getUserDetails().getId()+"");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                DriversResponse postsResponse = (DriversResponse) response;
                final DriversAdapter teachersAdapter = new DriversAdapter(getActivity(),postsResponse.getDrivers());
                final RecyclerView programsList = rootView.findViewById(R.id.recycler_view);
                programsList.setLayoutManager(new LinearLayoutManager(getActivity()));
                programsList.setAdapter(teachersAdapter);

            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, DriversResponse.class);
    }
}