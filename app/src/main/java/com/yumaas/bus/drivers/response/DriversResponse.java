package com.yumaas.bus.drivers.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DriversResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("drivers")
	private List<DriversItem> drivers;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setDrivers(List<DriversItem> drivers){
		this.drivers = drivers;
	}

	public List<DriversItem> getDrivers(){
		return drivers;
	}
}