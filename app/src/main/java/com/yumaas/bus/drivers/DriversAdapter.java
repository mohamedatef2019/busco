package com.yumaas.bus.drivers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.bus.R;
import com.yumaas.bus.drivers.response.DriversItem;

import java.util.ArrayList;
import java.util.List;


public class DriversAdapter extends RecyclerView.Adapter<DriversAdapter.ViewHolder> {

    Context context;

    List<DriversItem> driversItems;




    public DriversAdapter(Context context,List<DriversItem>driversItems) {
        this.context = context;
        this.driversItems=driversItems;
    }


    @Override
    public DriversAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_driver, parent, false);
        DriversAdapter.ViewHolder viewHolder = new DriversAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DriversAdapter.ViewHolder holder, final int position) {

        holder.name.setText("Name : "+driversItems.get(position).getName());
        holder.details.setText("Phone : "+driversItems.get(position).getPhone()+"\n"+
                "Email : "+driversItems.get(position).getEmail());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return driversItems==null?0:driversItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView name,details;

        public ViewHolder(View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            details=itemView.findViewById(R.id.details);
            imageView=itemView.findViewById(R.id.image);
        }
    }
}
