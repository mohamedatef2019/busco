package com.yumaas.bus.login;

import com.google.gson.annotations.SerializedName;

public class UserResponse{

	@SerializedName("state")
	private int state;

	@SerializedName("user")
	private UserItem user;

	public void setState(int state){
		this.state = state;
	}

	public int getState(){
		return state;
	}

	public void setUser(UserItem user){
		this.user = user;
	}

	public UserItem getUser(){
		return user;
	}

	@Override
 	public String toString(){
		return 
			"UserResponse{" + 
			"state = '" + state + '\'' + 
			",user = '" + user + '\'' + 
			"}";
		}
}