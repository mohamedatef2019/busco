package com.yumaas.bus.login;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.yumaas.bus.DriverMainActivity;
import com.yumaas.bus.R;
import com.yumaas.bus.UserMainActivity;
import com.yumaas.bus.base.UserPreferenceHelper;
import com.yumaas.bus.base.Validate;
import com.yumaas.bus.base.volleyutils.ConnectionHelper;
import com.yumaas.bus.base.volleyutils.ConnectionListener;
import com.yumaas.bus.noneed.CompanyMainActivity;
import com.yumaas.bus.register.RegisterActivity;
import com.yumaas.bus.register.RegisterAsCompanyActivity;

public class LoginActivity extends AppCompatActivity {
    EditText email,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email=findViewById(R.id.email);
        password=findViewById(R.id.password);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 342);
        }
        clickListeners();

    }

    private void clickListeners(){
        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Validate.isEmpty(email.getText().toString())||Validate.isEmpty(password.getText().toString())){
                    Toast.makeText(LoginActivity.this, "please fill all fields", Toast.LENGTH_SHORT).show();
                }else {
                    loginApi();
                }
            }
        });


        findViewById(R.id.rl_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                builder1.setMessage("You want to register as ");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Company",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                startActivity(new Intent(LoginActivity.this, RegisterAsCompanyActivity.class));
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        "User",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));

                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }


    public void loginApi(){

        LoginRequest loginRequest = new LoginRequest("login");
        loginRequest.setEmail(email.getText().toString());
        loginRequest.setPassword(password.getText().toString());

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                UserResponse userResponse = (UserResponse)response;
                if (userResponse.getState()==101){
                    UserPreferenceHelper.saveUserDetails(userResponse.getUser());

                    if(userResponse.getUser().getType().equals("2")){
                        startActivity(new Intent(LoginActivity.this, CompanyMainActivity.class));
                    }else if(userResponse.getUser().getType().equals("3")){
                        startActivity(new Intent(LoginActivity.this, DriverMainActivity.class));
                    }else if(userResponse.getUser().getType().equals("1")){
                        startActivity(new Intent(LoginActivity.this, UserMainActivity.class));
                    }
                }else {
                    Toast.makeText(LoginActivity.this, "Sorry wrong password", Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, UserResponse.class);
    }
}