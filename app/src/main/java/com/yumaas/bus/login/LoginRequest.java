package com.yumaas.bus.login;

import com.google.gson.annotations.SerializedName;

import com.yumaas.bus.base.DefaultRequest;


public class LoginRequest extends DefaultRequest {



    @SerializedName("email")
    private String email ;

    @SerializedName("password")
    private String password ;

    public LoginRequest(String method) {
        super(method);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
