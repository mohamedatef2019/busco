package com.yumaas.bus;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;
import com.yumaas.bus.aboutus.AboutUsFragment;
import com.yumaas.bus.addtrip.AddTripFragment;
import com.yumaas.bus.noneed.FragmentHelper;
import com.yumaas.bus.noneed.NotficationsFragment;
import com.yumaas.bus.register.AddDriverFragment;
import com.yumaas.bus.trips.DriverTripsFragment;
import com.yumaas.bus.trips.TripsFragment;
import com.yumaas.bus.trips.UserOldTripsFragment;
import com.yumaas.bus.trips.UserTripsFragment;

public class UserMainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    NavigationView navigationView;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);
        navigationView = findViewById(R.id.nav_view);
        drawer = findViewById(R.id.drawer_layout);


        findViewById(R.id.notifications).setOnClickListener(view -> FragmentHelper.addFragment(UserMainActivity.this, new NotficationsFragment(), "NotficationsFragment"));

        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 342);

        navigationView.setNavigationItemSelectedListener(this);


        FragmentHelper.addFragment(this, new UserTripsFragment(), "UserTripsFragment");
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        drawer.closeDrawer(GravityCompat.START);

        if (id != R.id.nav_share && id != R.id.nav_log_out && id != R.id.nav_rate) {
            FragmentHelper.popAllFragments(this);
        }

        if (id == R.id.nav_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_rate) {
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        } else if (id == R.id.nav_old_trips) {
            FragmentHelper.replaceFragment(this, new UserOldTripsFragment(), "UserOldTripsFragment");
        }else if (id == R.id.nav_home) {
            FragmentHelper.replaceFragment(this, new UserTripsFragment(), "UserTripsFragment");
        }else if (id == R.id.nav_trips) {
            FragmentHelper.replaceFragment(this, new UserTripsFragment(), "UserTripsFragment");
        }else if (id == R.id.nav_add_trip) {
            FragmentHelper.replaceFragment(this, new UserOldTripsFragment(), "UserOldTripsFragment");
        }

        else if (id == R.id.nav_about) {
            FragmentHelper.replaceFragment(this, new AboutUsFragment(), "AboutUsFragment");
        } else if (id == R.id.nav_log_out) {
            finish();
        }


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_main_container);
        fragment.onActivityResult(requestCode, resultCode, data);
    }
}
