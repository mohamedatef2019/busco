package com.yumaas.bus.register;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.yumaas.bus.R;
import com.yumaas.bus.base.UserPreferenceHelper;
import com.yumaas.bus.base.Validate;
import com.yumaas.bus.base.volleyutils.ConnectionHelper;
import com.yumaas.bus.base.volleyutils.ConnectionListener;
import com.yumaas.bus.login.UserResponse;
import com.yumaas.bus.noneed.CompanyMainActivity;

public class RegisterAsCompanyActivity extends AppCompatActivity {
    EditText name,phone,email,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        name=findViewById(R.id.name);
        phone=findViewById(R.id.phone);

        name.setHint("Company Name");
        email.setHint("Company Email");
        phone.setHint("Company Phone");

        clickListeners();

    }

    private void clickListeners(){
        findViewById(R.id.register).setOnClickListener(view -> {
            if(Validate.isEmpty(email.getText().toString())||Validate.isEmpty(password.getText().toString())){
                Toast.makeText(RegisterAsCompanyActivity.this, "please fill all fields", Toast.LENGTH_SHORT).show();
            }else {
                registerApi();
            }
        });
    }


    public void registerApi(){

        RegisterRequest loginRequest = new RegisterRequest("register");
        loginRequest.setEmail(email.getText().toString());
        loginRequest.setPassword(password.getText().toString());
        loginRequest.setName(name.getText().toString());
        loginRequest.setPhone(phone.getText().toString());
        loginRequest.setType("2");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                UserResponse userResponse = (UserResponse)response;
                if (userResponse.getState()==101){
                    UserPreferenceHelper.saveUserDetails(userResponse.getUser());
                    startActivity(new Intent(RegisterAsCompanyActivity.this, CompanyMainActivity.class));
                }else {
                    Toast.makeText(RegisterAsCompanyActivity.this, "Sorry  this phone or email used before", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, UserResponse.class);
    }


}