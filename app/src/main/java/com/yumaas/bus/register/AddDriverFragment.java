package com.yumaas.bus.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.yumaas.bus.R;
import com.yumaas.bus.base.UserPreferenceHelper;
import com.yumaas.bus.base.Validate;
import com.yumaas.bus.base.volleyutils.ConnectionHelper;
import com.yumaas.bus.base.volleyutils.ConnectionListener;
import com.yumaas.bus.drivers.DriversFragment;
import com.yumaas.bus.login.UserResponse;
import com.yumaas.bus.noneed.CompanyMainActivity;
import com.yumaas.bus.noneed.FragmentHelper;

public class AddDriverFragment extends Fragment {
    EditText name,phone,email,password;
    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_driver, container, false);

        email=rootView.findViewById(R.id.email);
        password=rootView.findViewById(R.id.password);
        name=rootView.findViewById(R.id.name);
        phone=rootView.findViewById(R.id.phone);

        clickListeners();

        return rootView;
    }


    private void clickListeners(){
        rootView.findViewById(R.id.register).setOnClickListener(view -> {
            if(Validate.isEmpty(email.getText().toString())||Validate.isEmpty(password.getText().toString())){
                Toast.makeText(requireActivity(), "please fill all fields", Toast.LENGTH_SHORT).show();
            }else {
                registerApi();
            }
        });
    }


    public void registerApi(){

        RegisterRequest loginRequest = new RegisterRequest("register");
        loginRequest.setEmail(email.getText().toString());
        loginRequest.setPassword(password.getText().toString());
        loginRequest.setName(name.getText().toString());
        loginRequest.setType("3");
        loginRequest.setCompanyId(UserPreferenceHelper.getUserDetails().getId()+"");
        loginRequest.setPhone(phone.getText().toString());

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                UserResponse userResponse = (UserResponse)response;
                if (userResponse.getState()==101){
                    FragmentHelper.popAllFragments(requireActivity());
                    FragmentHelper.replaceFragment(requireActivity(), new DriversFragment(), "DriversFragment");
                }
            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, UserResponse.class);
    }


}