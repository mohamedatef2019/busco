package com.yumaas.bus.register;

import com.google.gson.annotations.SerializedName;
import com.yumaas.bus.base.DefaultRequest;


public class RegisterRequest extends DefaultRequest {


    @SerializedName("name")
    private String name;


    @SerializedName("type")
    private String type;

    @SerializedName("email")
    private String email;

    @SerializedName("phone")
    private String phone;

    @SerializedName("password")
    private String password;


    @SerializedName("company_id")
    private String companyId="0";

    public RegisterRequest(String method) {
        super(method);
    }


    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    public String getPhone() { return phone; }
    public void setPhone(String phone) { this.phone = phone; }

    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyId() {
        return companyId;
    }
}
