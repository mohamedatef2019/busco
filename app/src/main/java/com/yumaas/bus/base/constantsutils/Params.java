package com.yumaas.bus.base.constantsutils;

public class Params {


    //IntentParams

    public static final String INTENT_PAGE = "page";
    public static final String INTENT_BUNDLE = "bundle";
    public static final String BUNDLE_ID = "id";
    public static final String BUNDLE_PRODUCT_DETAILS = "product_details";
    public static final String BUNDLE_PROGRAM_DETAILS = "program_details";
    public static final String BUNDLE_MY_PROGRAM_DETAILS = "my_program_details";
    public static final String BUNDLE_VIDEO_DETAILS = "video_details";



}
