package com.yumaas.bus.base.constantsutils;

public class Codes {

    //RequestsCodes
    public static int FILE_TYPE_IMAGE = 10;
    public static int FILE_TYPE_PDF = 12;


    //ACTIONS
    public static int REGISTER = 1 ;
    public static int LOGIN_SCREEN = 2;
    public static int HOME_PAGE = 3 ;
    public static int PROGRAM_DETAILS = 4 ;
    public static int GENDER_PAGE = 5 ;
    public static int CURRENT_BODY_PAGE = 6 ;
    public static int TARGET_BODY_PAGE = 7 ;
    public static int WEIGHT_PAGE = 8 ;
    public static int TRAIN_TIME_PAGE = 9 ;
    public static int TRAIN_MIN_PAGE = 10 ;
    public static int LEVEL_PAGE = 11 ;
    public static int HAVE_PROBLEM_PAGE = 12 ;
    public static int BODY_TARGET_PAGE = 13 ;
    public static int DIET_PLAN_DETAILS = 14 ;
    public static int EXERCISES_DETAILS_PAGE = 15;
    public static int CAPTION_DETAILS=16;
    public static int PACKAGES=17;
    public static int MY_PROGRAM_DETAILS=18;
    public static int EXERCISES_PAGE=19;
    public static int DIET_PLAN_PAGE=20;
    public static int SUPPLEMENTS_PAGE=21;
    public static int ADD_POST_PAGE=22;





}
