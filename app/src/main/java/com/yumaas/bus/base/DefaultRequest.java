package com.yumaas.bus.base;

import com.google.gson.annotations.SerializedName;

public class DefaultRequest {

    @SerializedName("method")
    private String method;
    @SerializedName("language")
    private String language;

    public DefaultRequest( ){

        this.language= UserPreferenceHelper.getCurrentLanguage();
    }

    public DefaultRequest(String method){
        this.method=method;
        this.language=UserPreferenceHelper.getCurrentLanguage();
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getMethod() {
        return method;
    }

}
