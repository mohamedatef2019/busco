package com.yumaas.bus.base;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.Objects;


public class BaseFragment extends Fragment {
    Context context;


    AlertDialog dialog;

    @SuppressLint("StaticFieldLeak")
    public void accessLoadingBar(int visiablity) {
//        try {
//            Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//
//                    if (visiablity == View.VISIBLE) {
//                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
//                        LayoutInflater inflater = BaseFragment.this.getLayoutInflater();
//                        View dialogView = inflater.inflate(R.layout.load, null);
//                        dialogBuilder.setView(dialogView);
//                        dialog = dialogBuilder.create();
//                        dialog.setCancelable(false);
//                        dialog.setCanceledOnTouchOutside(false);
//                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                    }
//
//
//                    if (visiablity == View.VISIBLE) {
//                        dialog.show();
//                    } else {
//
//
//                        dialog.hide();
//                        dialog.cancel();
//                        dialog.dismiss();
//
//                        new AsyncTask<Void, Void, Void>() {
//
//                            @Override
//                            protected Void doInBackground(Void... voids) {
//                                try {
//                                    Thread.sleep(1000);
//                                } catch (InterruptedException e) {
//                                    e.printStackTrace();
//                                }
//                                return null;
//                            }
//
//                            @Override
//                            protected void onPostExecute(Void aVoid) {
//                                super.onPostExecute(aVoid);
//                                try {
//                                    ((NavigationDrawerActivity) getActivity()).activityNavigationDrawerBinding.splashImage.setVisibility(View.GONE);
//                                } catch (Exception e) {
//                                    e.getStackTrace();
//                                }
//                            }
//                        }.execute();
//
//
//                    }
//
//                    //Your code to run in GUI thread here
//                }//public void run() {
//            });
//        }catch (Exception e){
//            e.getStackTrace();
//        }


    }

    public void showMessage(String message) {
        Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                //Your code to run in GUI thread here
            }//public void run() {
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.context = null;
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        this.context = context;
    }


}
