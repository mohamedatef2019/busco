package com.yumaas.bus.adduser;

//public  class AddUserFragment extends Fragment {
//
//    View rootView;
//    EditText name,phone,email,address;
//    RelativeLayout imageLayout;
//    ImageView imageView;
//    ArrayList<VolleyFileObject>volleyFileObjects;
//    Button addUserBtn;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        rootView = inflater.inflate(R.layout.fragment_new_driver, container, false);
//        requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},124);
//
//
//        email=rootView.findViewById(R.id.email);
//        address=rootView.findViewById(R.id.address);
//        name=rootView.findViewById(R.id.name);
//        imageLayout=rootView.findViewById(R.id.image_layout);
//        phone=rootView.findViewById(R.id.phone);
//        imageView=rootView.findViewById(R.id.image);
//        addUserBtn = rootView.findViewById(R.id.btn);
//
//        clickListener();
//
//        return rootView;
//    }
//
//    private void clickListener(){
//
//        addUserBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(volleyFileObjects==null){
//                    Toast.makeText(getActivity(), "Please add photo", Toast.LENGTH_SHORT).show();
//                }else if(Validate.isEmpty(name.getText().toString())
//                ||Validate.isEmpty(phone.getText().toString())
//                ||Validate.isEmpty(address.getText().toString())
//                ||Validate.isEmpty(email.getText().toString())){
//                    Toast.makeText(getActivity(), "Please fill all fields", Toast.LENGTH_SHORT).show();
//                }else {
//                    addUserApi();
//                }
//            }
//        });
//
//        imageLayout.setOnClickListener(view -> {
//            String[]choiceString = new String[]{"Gallery", "Camera"};
//            AlertDialog.Builder dialog = new AlertDialog.Builder(requireActivity());
//            dialog.setIcon(R.mipmap.ic_launcher);
//            dialog.setTitle("اختر من");
//            dialog.setItems(choiceString,
//                    (dialog1, which) -> {
//                        Intent intent;
//                        if (which == 0) {
//                            intent = new Intent(
//                                    Intent.ACTION_PICK,
//                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        } else {
//                            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        }
//                       startActivityForResult(Intent.createChooser(intent, "Select profile picture"), Codes.FILE_TYPE_IMAGE);
//                    }).show();
//        });
//    }
//
//
//    public void addUserApi(){
//
//        AddUserRequest addUserRequest = new AddUserRequest("add_user");
//        addUserRequest.setUserId(UserPreferenceHelper.getUserDetails().getId()+"");
//        addUserRequest.setEmail(email.getText().toString());
//        addUserRequest.setAddress(address.getText().toString());
//        addUserRequest.setName(name.getText().toString());
//        addUserRequest.setPhone(phone.getText().toString());
//        Random random = new Random();
//        addUserRequest.setCode(""+ random.nextInt(99999 - 10000 + 1) + 10000);
//
//        new ConnectionHelper(new ConnectionListener() {
//            @Override
//            public void onRequestSuccess(Object response) {
//                DefaultResponse userResponse = (DefaultResponse)response;
//                if (userResponse.getStatus()==101){
//                    startActivity(new Intent(requireActivity(), CompanyMainActivity.class));
//                    Toast.makeText(requireActivity(), "User added successfully", Toast.LENGTH_SHORT).show();
//
//                }else {
//                    Toast.makeText(requireActivity(), "Sorry can't add child", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onRequestError(Object error) {
//
//            }
//        }).multiPartConnect("",addUserRequest,volleyFileObjects, DefaultResponse.class);
//    }
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(volleyFileObjects==null)volleyFileObjects=new ArrayList<>();
//        try {
//            VolleyFileObject volleyFileObject =
//                    FileOperations.getVolleyFileObject(getActivity(), data, "image",
//                            Codes.FILE_TYPE_IMAGE);
//            volleyFileObjects.add(volleyFileObject);
//            assert volleyFileObject != null;
//            imageView.setImageBitmap(volleyFileObject.getCompressObject().getImage());
//        }catch (Exception e){
//            e.getStackTrace();
//        }
//    }
//
//}