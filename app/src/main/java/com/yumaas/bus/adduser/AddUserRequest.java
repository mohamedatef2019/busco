package com.yumaas.bus.adduser;

import com.google.gson.annotations.SerializedName;
import com.yumaas.bus.base.DefaultRequest;


public class AddUserRequest extends DefaultRequest {


    @SerializedName("name")
    private String name;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("code")
    private String code;

    @SerializedName("lat")
    private String lat="0.0";

    @SerializedName("lng")
    private String lng="0.0";

    @SerializedName("email")
    private String email;

    @SerializedName("phone")
    private String phone;

    @SerializedName("address")
    private String address;



    public AddUserRequest(String method) {
        super(method);
    }


    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    public String getPhone() { return phone; }
    public void setPhone(String phone) { this.phone = phone; }

    public String getAddress() { return address; }
    public void setAddress(String address) { this.address = address; }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLat() {
        return lat;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLng() {
        return lng;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }
}
